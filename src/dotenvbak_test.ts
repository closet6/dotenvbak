import { exists } from "https://deno.land/std/fs/mod.ts";
import { assert } from "https://deno.land/std@0.98.0/testing/asserts.ts";

Deno.test({
  name: "does dotenvbak dir exist?",
  fn: async () => {
    assert(await exists("/home/joe/.dotenvbak"));
  },
});
