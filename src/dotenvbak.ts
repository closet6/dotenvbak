import getFiles from "https://deno.land/x/getfiles@v1.0.0/mod.ts";
import { parse } from "https://deno.land/std@0.98.0/flags/mod.ts";
import { exists } from "https://deno.land/std/fs/mod.ts";

const args = parse(Deno.args);

let outputDir = "";
const primeDir = ".dotenvbak";

if (args.d) {
  outputDir = args.d;
  if (!exists(outputDir)) {
    throw Error("you passed a directory that doesn't exist");
  }
} else {
  const defaultBaseDir = findHome();
  outputDir = defaultBaseDir + "/" + primeDir;
  if (!(await exists(outputDir))) {
    await Deno.mkdir(outputDir);
  }
}

const fp = `${outputDir}/${Date.now().toString()}.env.txt`;
let data = "";

const files = getFiles({
  root: "./",
  exclude: [".git", ".svn"],
});

for (const f of files) {
  if (f.name.substring(0, 4) === ".env") {
    data += await Deno.readTextFile(f.realPath);
  }
}

if (data) {
  await Deno.writeTextFile(fp, data);
  console.log(`wrote to file: ${fp} \nthis data: \n${data}`);
} else {
  console.log("no .env files found, so no file was written");
}

function findHome() {
  let homeDir = Deno.env.get("HOME");
  if (homeDir) return homeDir;
  homeDir = Deno.env.get("HOMEPATH");
  if (homeDir) return homeDir;
  throw Error(
    "No base directory can be found, use custom dir with 'dotenvbak -d ~/somedir`"
  );
}
