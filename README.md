# dotenvbak

## summary

walks your current directory and looks for `.env*` files and reads the contents off all of them, concatenates the data, and writes it to `$HOME/.dotenvbak/{TIEMSTAMP}.env.txt`

## why?

I have at least one or a couple .env files in every project, I have a lot of projects, have several computers I work on between job and home, I travel, and I don't check .env files into git. I'd like to get a quick no thinking backup copy of of the values.

## install

- install deno: https://deno.land/manual/getting_started/installation
- install dotenvbak: `deno install --allow-read --allow-write --allow-env -f -n dotenvbak https://gitlab.com/closet6/dotenvbak/-/raw/main/src/dotenvbak.ts`

## run

- cd to top level of project directory
  - `dotenvbak` (writes dotenvbak files to $HOME/.dotenvbak)
  - `dotenvbak -d ~/customenvbakfolder` (writes files to ~/customenvbakfolder)

## todo

- more proffesional testing
- have file naming convention include project name
- test on windows
- optional config?
